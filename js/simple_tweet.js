(function ($) {
  Drupal.behaviors.simple_tweet = { attach: function (context, settings) {
      if(Drupal.settings.simple_tweet_id != null){
       tweet();
      }
    }
  };
  tweet = function(context){
    $.ajax({
      url: '?q=simple_tweet_block_content/simple_tweet_'+Drupal.settings.simple_tweet_id.id,
      success: function(result) {
       $('div.twitter-updates').replaceWith($(result).find('div.content').html());
      }
    });
    setTimeout("tweet()",Drupal.settings.simple_tweet_id.tweets_reload_time*60*1000);
  }
}(jQuery));
