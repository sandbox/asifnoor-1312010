<?php
// $Id$

/**
 * @file
 * Theme template for a list of tweets.
 */
?>
<div class="twitter-updates">
<?php 
foreach ($tweets as $k => $v) {?>
  <div style="padding-top:8px;border-bottom:1px solid #818196;">
     <span class="author"><?php print l(check_plain($v['author']), $v['author_uri']);?>:</span>
     <span class="content"><?php print check_markup($v['desc']);?></span>
     <span class="published-date">Posted on:<?php print $v['date']; ?></span>
  </div>
<?php } ?>
</div>
