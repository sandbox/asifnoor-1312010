<?php
// $Id$

/**
 * @file
 * Simple Tweet module.
 */

define ("SIMPLE_TWEET_URL", "http://search.twitter.com/search.atom?q=");

/**
  * Implement hook_help()
  */
function simple_tweet_help($path, $arg) {
 switch ($path) {
 case 'admin/help#simple_tweet':
   $output = '';
   $output .= '<h3>' . t('About Simple tweet') . '</h3>';
   $output .= '<p>' . t("Pull Tweets from Twitter site and display them as a block.") . '<p>';
   $output .= '<h3>' . t('Search twitter keywords') . '</h3>';
   $output .= '<p>' . t('<table style="width:70%">
	<tr><th>Operator / Keyword</th><th>Finds tweets...</th></tr>
	<tr><td>twitter search</td><td>containing both "twitter" and "search". This is the default operator.</td></tr>
	<tr><td>"happy hour"</td><td>containing the exact phrase "happy hour".</td></tr>
	<tr><td>love <strong>OR</strong> hate</td><td>containing either "love" or "hate" (or both).</td></tr>
	<tr><td>beer -root</td><td>containing "beer" but not "root".</td></tr>
	<tr><td>#haiku</td><td>containing the hashtag "haiku".</td></tr>
	<tr><td><strong>from:</strong>alexiskold</td><td>sent from person "alexiskold".</td></tr>
	<tr><td><strong>to:</strong>techcrunch</td><td>sent to person "techcrunch".</td></tr>
	<tr><td>@mashable</td><td>referencing person "mashable".</td></tr>
	<tr><td>"happy hour" <strong>near:</strong>"san francisco"</td><td>containing the exact phrase "happy hour" and sent near "san francisco".</td></tr>
	<tr><td><strong>near:</strong>NYC <strong>within:</strong>15mi</td><td>sent within 15 miles of "NYC".</td></tr>
	<tr><td>superhero since:2010-12-27</td><td>containing "superhero" and sent since date "2010-12-27" (year-month-day).</td></tr>
	<tr><td>ftw <strong>until:</strong>2010-12-27</td><td>containing "ftw" and sent up to date "2010-12-27".</td></tr>
	<tr><td>movie -scary :)</td><td>containing "movie", but not "scary", and with a positive attitude.</td></tr>
	<tr><td>flight :(</td><td>containing "flight" and with a negative attitude.</td></tr>
	<tr><td>traffic ?</td><td>containing "traffic" and asking a question.</td></tr>
	<tr><td>hilarious <strong>filter:links</strong></td><td>containing "hilarious" and linking to URLs.</td></tr>
	<tr><td>news <strong>source:twitterfeed</td><td>containing "news" and entered via TwitterFeed</td></tr>
</table>') . '<p>';
   $output .= '<p>' . t("To know more information about operators or search keywords") . ' ' . l(t('click here'), 'http://twitter.com/#!/search-home', array('attributes' => array('target' => '_blank'))) . '<p>';

 return $output;
   }
 }

/**
 * Implementation of hook_menu()
 */
function simple_tweet_menu() {
  $items = array();

  $items['admin/structure/simple_tweet'] = array(
    'title' => 'Simple Tweets',
    'description' => 'Create blocks of Simple Tweets.',
    'page callback' => 'simple_tweet_list',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'simple_tweet.admin.inc',
    'weight' => -10,
  );

  $items['admin/structure/simple_tweet/list'] = array(
    'title' => 'Simple Tweets List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/structure/simple_tweet/add'] = array(
    'title' => 'Add Simple Sweet block',
    'description' => 'Configuring the Simple Tweet Settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('config_simple_tweet_settings', 'add'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_ACTION,
    'weight' => 1,
  );

  $items['admin/structure/simple_tweet/manage/%simple_tweet'] = array(
    'title' => 'Edit Simple Tweet',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('config_simple_tweet_settings', 'edit', 4),
    'access arguments' => array('administer site configuration'),
    );
  
  $items['admin/structure/simple_tweet/manage/%simple_tweet/edit'] = array(
    'title' => 'Edit Simple Tweet',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  
  $items['admin/structure/simple_tweet/manage/%simple_tweet/delete'] = array(
    'title' => 'Delete Simple tweet',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('simple_tweet_block_delete', 4),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'simple_tweet.admin.inc',
  );
  $items['simple_tweet_block_content'] = array(
    'page callback' => 'simple_tweet_block_content_reload',
    'access arguments' => array('access content'),
    //'page arguments' => array('delta'=>'simple_tweet_2'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Load the simple tweet data.
 */
function simple_tweet_load($id) {
  $content = array();
  $query = db_select('simple_tweet', 'sd')->fields('sd', array('twitter_keyword', 'cache_time_limit', 'tweet_limit', 'tweets_reload_time'))->condition('sd.id', $id, '=');
  $simple_tweet = $query->execute();
  if (!$simple_tweet) {
    return FALSE;
  }
  foreach ($simple_tweet as $record) {
    $content['id'] = $id;
    $content['twitter_keyword'] = $record->twitter_keyword;
    $content['cache_time_limit'] = $record->cache_time_limit;
    $content['tweet_limit'] = $record->tweet_limit;
    $content['tweets_reload_time'] = $record->tweets_reload_time;
  }

  return $content;
}

/*
 *  Implementation of hook_theme()
 */
function simple_tweet_theme() {
  return array(
    'simple-tweet-listing' => array(
      'variables' => array('tweets' => NULL),
      'template' => 'simple-tweet-listing'
     )
  );
}
/*
 * Simple tweet settings form
 */
function config_simple_tweet_settings($form, &$form_state, $formtype, $simple_tweet = NULL) {
  $default_values = $simple_tweet;
  $form = array();

  if ($formtype == 'edit') {
    $form['id'] = array(
      '#type' => 'hidden',
      '#value' => isset($simple_tweet['id']) ? $simple_tweet['id'] : 0,
    );
  }

  $form['twitter_keyword'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter Search Word'),
    '#default_value' => $default_values['twitter_keyword'],
    '#description' => t('Use') . ' ' . l(t('operators'), 'admin/help/simple_tweet') . ' ' . t('for advanced search.'),
    '#maxlength' => 64,
    '#required' => TRUE
    );

  $form['cache_time_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache Limit'),
    '#default_value' => $default_values['cache_time_limit'],
    '#description' => t('Tweets cache time limit in minutes.'),
    '#maxlength' => 5,
    '#required' => TRUE
    );

  $form['tweet_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('No.of tweets'),
    '#default_value' => $default_values['tweet_limit'],
    '#description' => t('No.of tweets to display.'),
    '#maxlength' => 2, 
    '#required' => TRUE
    );
  $form['tweets_reload_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Tweets reload time'),
    '#default_value' => $default_values['tweets_reload_time'],
    '#description' => t('Tweets reload time (In minutes).'),
    '#maxlength' => 5,
    '#required' => TRUE
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['#validate'][] = 'simple_tweet_form_validate';
  return $form;
}

/*
 * Simple tweet block validations 
*/
function simple_tweet_form_validate($form, &$form_state) {

 if (!is_numeric($form_state['values']['cache_time_limit'])) {
  form_set_error('cache_time_limit', t('Please enter numeric value for cache limit.'));
  return FALSE;
 } 
 elseif (!is_numeric($form_state['values']['tweet_limit'])) {
  form_set_error('tweet_limit', t('Please enter numeric value for tweets limit.'));
  return FALSE;
 }
}

/*
 *simple tweet addition form settings submit 
*/
function config_simple_tweet_settings_submit($form, $form_state) {
  $values = $form_state['values'];

  $twitter_keyword = $values['twitter_keyword'];
  $tweet_limit = $values['tweet_limit'];
  $cache_time_limit = $values['cache_time_limit'];
  $tweets_reload_time = $values['tweets_reload_time'];

  $id = '';
  if (array_key_exists('id', $values))
  $id = $values['id'];

  if (!$id) {
    $insert = db_insert('simple_tweet')
      ->fields(array('twitter_keyword', 'tweet_limit', 'cache_time_limit', 'tweets_reload_time'))
      ->values(array('twitter_keyword' => $twitter_keyword, 'tweet_limit' => $tweet_limit,
        'cache_time_limit' => $cache_time_limit, 'tweets_reload_time' => $tweets_reload_time
      ))
      ->execute();
    if ($insert) {
      drupal_set_message(t("Added simple tweet block sucessfully"));
      drupal_goto('admin/structure/simple_tweet');
    }
  }
  else {
    $update = db_update('simple_tweet')
      ->fields(array(
        'twitter_keyword' => $twitter_keyword,
        'tweet_limit' => $tweet_limit,
        'cache_time_limit' => $cache_time_limit,
        'tweets_reload_time' => $tweets_reload_time,
        ))
      ->condition('id', $id, '=')
      ->execute();
    if ($update) {
      drupal_set_message(t("Updated simple tweet block sucessfully"));
      drupal_goto('admin/structure/simple_tweet');
    }
  }
}

/*
 * Implementing hook block info
 */
function simple_tweet_block_info() {
  $blocks = array();
  $query = db_select("simple_tweet", 'sd')->fields('sd', array('id', 'twitter_keyword'))->execute();
  
  foreach ($query as $simple_tweet_block) {
    $blocks['simple_tweet_' . $simple_tweet_block->id] = array('info' => t($simple_tweet_block->twitter_keyword));
  }
  return $blocks;
}

/*
 *Implementing hook_block_view
 */
function simple_tweet_block_view($delta = '') {
  $block['subject'] = t('');
  $block['content'] = simple_tweet_block_content($delta);
  return $block;
}

/*
 *Implementing hook_block_content
 */
function simple_tweet_init() {
  drupal_add_js(drupal_get_path('module', 'simple_tweet') . '/js/simple_tweet.js');
}

function simple_tweet_block_content_reload($delta) {
  $id = array_pop(explode('_', $delta));
  $simple_tweet_data = simple_tweet_load($id);
  if ($delta == "simple_tweet_" . $simple_tweet_data['id'] &&  !empty($simple_tweet_data['twitter_keyword'])) {    
    drupal_add_js(array('simple_tweet_id' => $simple_tweet_data), 'setting');
    $tweets = fetch_tweets($simple_tweet_data['twitter_keyword'], $simple_tweet_data['tweet_limit']);
    if (!empty($tweets)) {
       $output = theme('simple-tweet-listing', array('tweets' => $tweets));
    }
    else {
       $output = t('No Tweet results for ' . $simple_tweet_data['twitter_keyword']);
     }      
     cache_set('simple_tweet_data_' . $simple_tweet_data['id'], $output, 'cache', time() + (60 * $simple_tweet_data['cache_time_limit']));
  }
  return $output;
}

function simple_tweet_block_content($delta) {
  $id = array_pop(explode('_', $delta));
  $simple_tweet_data = simple_tweet_load($id);
  if ($delta == "simple_tweet_" . $simple_tweet_data['id'] &&  !empty($simple_tweet_data['twitter_keyword'])) {    
    //Fetch the data from cache
    drupal_add_js(array('simple_tweet_id' => $simple_tweet_data), 'setting');
    if ($cache = cache_get('simple_tweet_data_' . $simple_tweet_data['id'])) {
      $output = $cache->data;
    }
    else {
      $tweets = fetch_tweets($simple_tweet_data['twitter_keyword'], $simple_tweet_data['tweet_limit']);
      if (!empty($tweets)) {
        $output = theme('simple-tweet-listing', array('tweets' => $tweets));
      }
      else {
        $output = t('No Tweet results for ' . $simple_tweet_data['twitter_keyword']);
      }      
      cache_set('simple_tweet_data_' . $simple_tweet_data['id'], $output, 'cache', time() + (60 * $simple_tweet_data['cache_time_limit']));
    }
    return $output;
  } 
}


/*
 *  Fetch tweets using twitter search keyword
 */
function fetch_tweets($keyword, $maxtweets) {
  //Using simplexml to load URL
  $tweets = simplexml_load_file(SIMPLE_TWEET_URL . urlencode($keyword));
  if ($tweets) {
    $tweet_array = array(); 
    foreach ($tweets->entry as $key => $tweet) { 
      if ($maxtweets == 0) {
        break;
      }
      else {
        $tweet = (array)$tweet;
        $pubdate = strtotime($tweet['published']);
        $tweet_item = array(
         'desc' => $tweet['content'],
         'date' => gmdate('F jS Y, H:i', $pubdate),
         'author' => $tweet['author']->name,
         'author_uri' => $tweet['author']->uri,
       );
      array_push($tweet_array, $tweet_item);
      $maxtweets--;
      }
    }
    return $tweet_array;
  }
}
