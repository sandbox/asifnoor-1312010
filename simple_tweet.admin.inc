<?php
// $Id$

/**
 * @file
 * Add, Update and Delete block functions for the simple tweet module.
 */

function simple_tweet_list() {
  $result = db_query('SELECT id, twitter_keyword FROM {simple_tweet} ORDER BY id ASC');
  $header = array(
    array('data' => t('Simple Tweet Blocks')),
    array('data' => t('Operations'), 'colspan' => 3),
  );
  $rows = array();
  foreach ($result as $row) {
    $tablerow = array(
      array('data' => $row->twitter_keyword),
      array('data' => l(t('Edit'), 'admin/structure/simple_tweet/manage/' . $row->id . '/edit')),
      array('data' => l(t('Delete'), 'admin/structure/simple_tweet/manage/' . $row->id . '/delete')),
    );
    $rows[] = $tablerow;
  }
  if (!$rows) {
    $rows[] = array(array('data' => t('No Simple Tweet blocks available.'), 'colspan' => 4));
  }

  $build = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'simple_tweet'),
  );
  return $build;
}

/**
 * Deletion of simple tweet block.
 */
function simple_tweet_block_delete($form, $form_state, $simple_tweet) {
  $form['id'] = array('#type' => 'hidden', '#value' => $simple_tweet['id']);
  $form['sd_name'] = array('#type' => 'hidden', '#value' => $simple_tweet['twitter_keyword']);
  return confirm_form($form, t('Are you sure you want to delete the simple tweet block "%name"?', array('%name' => $simple_tweet['twitter_keyword'])), 'admin/structure/simple_tweet', '', t('Delete'), t('Cancel'));
}

/**
 * Submit handler for simple tweet block deletion.
 */
function simple_tweet_block_delete_submit($form, &$form_state) {
  $delta = $form_state['values']['sd_name'] . "_" . $form_state['values']['id'];
  $simple_tweet_delete = db_delete('simple_tweet')->condition('id', $form_state['values']['id'])->execute();
  $and = db_and()->condition('module', 'simple_tweet')->condition('delta', $delta);
  $simple_tweet_block_delete = db_delete('block')->condition($and)->execute();
  drupal_set_message(t('The Simple Tweet block "%name" has been removed.', array('%name' => $form_state['values']['sd_name'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/simple_tweet';
};
    
