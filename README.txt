This is a simple module which will provide a block consisting of tweets based on different search criteria terms. Using this module we can create as many tweets block as we want on different #tags.

It retrieve tweets from twitter and display them in a block. The module supports caching, so that the tweets get cached for the specified cache time.

It will not store the tweets as nodes, it will retrieve tweets and display. 

Installation Instructions
-------------------------
1. Download and unpack the module.
2. Place the module in your modules folder (this will usually be "sites/all/modules/").
3. Enable the module under admin/modules

Usage:
------
1. To create a new tweets block go to "Configuration" -> "Structure" -> "Simple Tweets" -> "Add Simple Tweet Block".

2. Enter the search keyword, based on this keyword your tweets will display. Some of the search keyword examples are
   a. #drupalcon - Retrieve the tweets with hash tag drupalcon
   b. @aquia - Retrieve the tweets which are referencing aquia
   c. from:azrisolutions - Retrieve the tweets sent from azrisolutions
   For more seach keyword options check the path admin/help/simple_tweet

3. Enter the cache limit in minutes. Until this limit the tweets retrieved will get cached. 

4. Enter the number of tweets to display in the block and submit, the simple tweets block will get created.
   You can see the created block in Admin > Structure > Blocks section.

5. Visit Admin > Structure > Blocks and configure the simple tweets block.

6. Enjoy creating more tweet blocks :)